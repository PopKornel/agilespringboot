package com.example.springbootapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class SpringBootAppApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootAppApplication.class, args);
    }
}

@RestController
class ListStoreController {
    private List<String> store = new ArrayList<>();

    @GetMapping("/store")
    public List<String> retrieveData() {
        return store;
    }

    @PostMapping("/store")
    public void storeData(@RequestBody String data) {
        store.add(data);
    }

    @PutMapping("/store/{index}")
    public void updateData(@PathVariable int index, @RequestBody String data) {
        store.set(index, data);
    }

    @DeleteMapping("/store/{index}")
    public void deleteData(@PathVariable int index) {
        store.remove(index);
    }
}
