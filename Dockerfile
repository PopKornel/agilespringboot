# Use an openJDK image as the base image
#FROM openjdk:14-jdk-alpine
FROM maven:3.6.1-jdk-8 as build

# Set the working directory
WORKDIR /app

# Copy the pom.xml file to the container
COPY pom.xml /app

# Run the Maven build to download dependencies
RUN mvn -f /app/pom.xml dependency:go-offline

# Copy the rest of the application code to the container
COPY . /app

# Build the application
RUN mvn -f /app/pom.xml clean install

# Expose the application port
EXPOSE 8080

# Run the application
CMD ["java", "-jar", "/app/target/app.jar"]
